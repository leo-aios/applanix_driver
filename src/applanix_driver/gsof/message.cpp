#include "applanix_driver/gsof/message.h"

#include <cstring>

namespace applanix_driver::gsof {

Message::Message(const std::byte *data, const std::size_t length) : data_(data), length_(length) {}

Header Message::getHeader() const {
  Header header;
  std::memcpy(&header, data_, sizeof(Header));
  return header;
}



}  // namespace applanix_driver::gsof
